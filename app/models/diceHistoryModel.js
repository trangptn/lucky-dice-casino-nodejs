const mongoose= require('mongoose');

const schema = mongoose.Schema;

const diceHistorySchema= new schema({
    user:[
        {
        type: mongoose.Types.ObjectId,
        ref:"user",
        require:true
        }       
    ],
    dice:{
        type:Number,
        require:true
    },
    createdAt:{
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default:Date.now()
    }
},
{
    timestamps:true
}
)
module.exports= mongoose.model("diceHistory",diceHistorySchema);