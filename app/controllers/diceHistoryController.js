const mongoose = require('mongoose');
const diceHistoryModel=require('../models/diceHistoryModel');

const createDiceHistory= async (req,res) =>{
    const reqDice=Math.floor(Math.random() *6)+1;

    if (!reqDice) {
        res.status(400).json({
            message: "Dice khong hop le"
        })
        return false;
    }
    try{
        var newDiceHistory={
            dice:reqDice,
        }

        const result = await diceHistoryModel.create(newDiceHistory);
        res.status(201).json({
            message:"Tao dice history thanh cong",
            data: result
        })

    }catch(err){
        return err.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllDiceHistory= async (req,res)=>{
    try{
        const result= await diceHistoryModel.find();
        return res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Co loi xay ra",
            data:result
        })
    }
}

const getDiceHistoryById= async (req,res)=>{
    const dicehistoryid= req.params.dicehistoryid;

    if(!mongoose.Types.ObjectId.isValid(dicehistoryid))
    {
        return res.status(400).json({
            message:" Dice history Id khong hop le"
        })
    }
    
    const result= await diceHistoryModel.findById(dicehistoryid);
    try{
       return res.status(200).json({
            message:" Lay du lieu thanh cong",
            data: result
       })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateDiceHistoryById= async (req,res) =>{
    const dicehistoryid=req.params.dicehistoryid;

    const{
        reqDice
    }=req.body;

    const reqUpdatedAt= Date.now();

     //B2: validate du lieu
    if (reqDice == "") {
        res.status(400).json({
            message: "Dice khong hop le"
        })
        return false;
    }
    try{
        var newUpdateDiceHistory= {};
        if(reqDice)
        {
            newUpdateDiceHistory.dice=reqDice;
        }
        newUpdateDiceHistory.updatedAt=reqUpdatedAt;

        const result= await diceHistoryModel.findByIdAndUpdate(dicehistoryid,newUpdateDiceHistory);

        if(result)
        {
            res.status(200).json({
                message:"Cap nhat thong tin thanh cong",
                data:result
            })
        }
        else
        {
            res.status(404).json({
                message:"Khong tim thay thong tin dice history"
            })
        }
    }
    catch(err)
    {
        res.return(500).json({
            message:"Co loi xay ra"
        })

    }
}

const deleteDiceHistoryById= async (req,res) =>{
    const dicehistoryid=req.params.dicehistoryid;

    if(!mongoose.Types.ObjectId.isValid(dicehistoryid)){
        res.status(400).json({
            message:"DiceHistory Id khong hop le"
        })
    }
    try{
        const result= await diceHistoryModel.findByIdAndDelete(dicehistoryid);

        if(result)
        {
            return res.status(200).json({
                message:"Xoa thong tin DiceHistory thanh cong"
            })
        }
        else
        {
            res.status(404).json({
                message:"Khong tim thay thong tin DiceHistory"
            })
        }
    }
    catch(err){
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports={
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}