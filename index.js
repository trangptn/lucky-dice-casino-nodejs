//import thu vien express
const express= require('express');

//khoi tao app express
const app= new express();

//cau hinh su dung json
app.use(express.json());
//Khai bao cong chay
const port=8000;

//import mongoose
const mongoose= require('mongoose');

mongoose.connect("mongodb://127.0.0.1:27017/lucky_dice")
.then(()=>{
    console.log("Successfully connected");
})
.catch((err)=>{
    console.log(err.message);
})

const randomRouter= require("./app/routers/randomRouter");

const userRouter=require('./app/routers/userRouter');

const diceHistoryRouter=require('./app/routers/diceHistory');

const prizeRouter=require("./app/routers/prizeRouter");

const voucherRouter=require("./app/routers/voucherRouter");


app.use("/",randomRouter);
app.use("/users",userRouter);
app.use("/diceHistorys",diceHistoryRouter);
app.use("/prizes",prizeRouter);
app.use("/vouchers",voucherRouter);

//start app
app.listen(port, ()=>{
    console.log(`app listening on port ${port}`);
})